import React, { useEffect } from 'react'
import * as actions from '../redux/actions'
import { connect } from 'react-redux'
const One = (props) => {
    const { count, name, increment, setName } = props;
    const onClickHandler = (params) => {
        increment();
        setName();
    }
    return (
        <div>
            Component 4<br />
            <hr />
            Count :{(count) ? count : 0} <br />
            <div><button onClick={onClickHandler}>Increase Counter</button></div>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        count: state.modalReducer.count,
        name: state.modalReducer.name
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setName: () => dispatch(actions.setValue('Incremented By Component 4')),
        increment: () => dispatch(actions.incrementCount())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(One);