import logo from './logo.svg';
import './App.css';
import One from './components/One'
import Two from './components/Two'
import Three from './components/Three'
import Four from './components/Four'
import { useEffect } from 'react';
import { connect } from 'react-redux'

const App = (props) => {
  const { count, name } = props;
  return (
    <div className="main">
      Store Values<br />
      <hr />
     Count : {(count)? count:0}<br />
     Remarks : {name}<br />
      <div className="app">
        <div className="components"><One /></div>
        <div className="components"><Two /></div>
        <div className="components"><Three /></div>
        <div className="components"><Four /></div>

      </div>
    </div>

  );
}

const mapStateToProps = state => {
  return {
    count: state.modalReducer.count,
    name: state.modalReducer.name
  }
}
export default connect(mapStateToProps)(App);
