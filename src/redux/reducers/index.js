import {combineReducers} from 'redux'
import modalReducer from '../../modal/services/modalReducer'
export default  combineReducers({
    modalReducer
})