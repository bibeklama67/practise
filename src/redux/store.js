import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from "redux-thunk"
import rootReducers from './reducers'

const middlware = [thunk];

const store = createStore(
    rootReducers,
    composeWithDevTools(applyMiddleware(...middlware))
);
export default store;